EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:TL072 U?
U 1 1 5DFDBE31
P 5050 2500
AR Path="/5DFDBE31" Ref="U?"  Part="1" 
AR Path="/5DF8C731/5DFDBE31" Ref="U1"  Part="1" 
F 0 "U1" H 5050 2867 50  0000 C CNN
F 1 "TL072" H 5050 2776 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 5050 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 5050 2500 50  0001 C CNN
	1    5050 2500
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL072 U?
U 2 1 5DFDBE37
P 5200 4150
AR Path="/5DFDBE37" Ref="U?"  Part="2" 
AR Path="/5DF8C731/5DFDBE37" Ref="U1"  Part="2" 
F 0 "U1" H 5200 4517 50  0000 C CNN
F 1 "TL072" H 5200 4426 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 5200 4150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 5200 4150 50  0001 C CNN
	2    5200 4150
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL072 U?
U 1 1 5DFDBE3D
P 7700 2500
AR Path="/5DFDBE3D" Ref="U?"  Part="1" 
AR Path="/5DF8C731/5DFDBE3D" Ref="U2"  Part="1" 
F 0 "U2" H 7700 2867 50  0000 C CNN
F 1 "TL072" H 7700 2776 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 7700 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 7700 2500 50  0001 C CNN
	1    7700 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 2600 4500 2600
Wire Wire Line
	4500 2600 4500 2850
Wire Wire Line
	4500 2850 5550 2850
Wire Wire Line
	5550 2850 5550 2500
Wire Wire Line
	5550 2500 5350 2500
Connection ~ 5550 2500
Wire Wire Line
	5550 2500 5850 2500
Wire Wire Line
	4900 4250 4650 4250
Wire Wire Line
	4650 4250 4650 4500
Wire Wire Line
	4650 4500 5750 4500
Wire Wire Line
	5750 4500 5750 4150
Wire Wire Line
	5750 4150 5500 4150
Connection ~ 5750 4150
Wire Wire Line
	5750 4150 6200 4150
$Comp
L Device:C C?
U 1 1 5DFDBE51
P 3950 2400
AR Path="/5DFDBE51" Ref="C?"  Part="1" 
AR Path="/5DF8C731/5DFDBE51" Ref="C6"  Part="1" 
F 0 "C6" V 3698 2400 50  0000 C CNN
F 1 "10nF" V 3789 2400 50  0000 C CNN
F 2 "" H 3988 2250 50  0001 C CNN
F 3 "~" H 3950 2400 50  0001 C CNN
	1    3950 2400
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5DFDBE57
P 6000 2500
AR Path="/5DFDBE57" Ref="C?"  Part="1" 
AR Path="/5DF8C731/5DFDBE57" Ref="C8"  Part="1" 
F 0 "C8" V 5748 2500 50  0000 C CNN
F 1 "10nF" V 5839 2500 50  0000 C CNN
F 2 "" H 6038 2350 50  0001 C CNN
F 3 "~" H 6000 2500 50  0001 C CNN
	1    6000 2500
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5DFDBE5D
P 6700 2500
AR Path="/5DFDBE5D" Ref="C?"  Part="1" 
AR Path="/5DF8C731/5DFDBE5D" Ref="C9"  Part="1" 
F 0 "C9" V 6448 2500 50  0000 C CNN
F 1 "10nF" V 6539 2500 50  0000 C CNN
F 2 "" H 6738 2350 50  0001 C CNN
F 3 "~" H 6700 2500 50  0001 C CNN
	1    6700 2500
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5DFDBE63
P 4250 4450
AR Path="/5DFDBE63" Ref="C?"  Part="1" 
AR Path="/5DF8C731/5DFDBE63" Ref="C7"  Part="1" 
F 0 "C7" H 4135 4404 50  0000 R CNN
F 1 "10n" H 4135 4495 50  0000 R CNN
F 2 "" H 4288 4300 50  0001 C CNN
F 3 "~" H 4250 4450 50  0001 C CNN
	1    4250 4450
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5DFDBE69
P 4250 2800
AR Path="/5DFDBE69" Ref="R?"  Part="1" 
AR Path="/5DF8C731/5DFDBE69" Ref="R5"  Part="1" 
F 0 "R5" H 4320 2846 50  0000 L CNN
F 1 "6k8" H 4320 2755 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4180 2800 50  0001 C CNN
F 3 "~" H 4250 2800 50  0001 C CNN
	1    4250 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DFDBE6F
P 6350 2100
AR Path="/5DFDBE6F" Ref="R?"  Part="1" 
AR Path="/5DF8C731/5DFDBE6F" Ref="R6"  Part="1" 
F 0 "R6" H 6420 2146 50  0000 L CNN
F 1 "R" H 6420 2055 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6280 2100 50  0001 C CNN
F 3 "~" H 6350 2100 50  0001 C CNN
	1    6350 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DFDBE75
P 7050 3050
AR Path="/5DFDBE75" Ref="R?"  Part="1" 
AR Path="/5DF8C731/5DFDBE75" Ref="R8"  Part="1" 
F 0 "R8" H 7120 3096 50  0000 L CNN
F 1 "6k8" H 7120 3005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6980 3050 50  0001 C CNN
F 3 "~" H 7050 3050 50  0001 C CNN
	1    7050 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DFDBE7B
P 8000 2800
AR Path="/5DFDBE7B" Ref="R?"  Part="1" 
AR Path="/5DF8C731/5DFDBE7B" Ref="R10"  Part="1" 
F 0 "R10" H 8070 2846 50  0000 L CNN
F 1 "10k" H 8070 2755 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7930 2800 50  0001 C CNN
F 3 "~" H 8000 2800 50  0001 C CNN
	1    8000 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DFDBE81
P 8000 3250
AR Path="/5DFDBE81" Ref="R?"  Part="1" 
AR Path="/5DF8C731/5DFDBE81" Ref="R11"  Part="1" 
F 0 "R11" H 8070 3296 50  0000 L CNN
F 1 "10k" H 8070 3205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7930 3250 50  0001 C CNN
F 3 "~" H 8000 3250 50  0001 C CNN
	1    8000 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DFDBE87
P 3550 5050
AR Path="/5DFDBE87" Ref="R?"  Part="1" 
AR Path="/5DF8C731/5DFDBE87" Ref="R3"  Part="1" 
F 0 "R3" H 3620 5096 50  0000 L CNN
F 1 "10k" H 3620 5005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3480 5050 50  0001 C CNN
F 3 "~" H 3550 5050 50  0001 C CNN
	1    3550 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DFDBE8D
P 8500 2500
AR Path="/5DFDBE8D" Ref="R?"  Part="1" 
AR Path="/5DF8C731/5DFDBE8D" Ref="R14"  Part="1" 
F 0 "R14" V 8293 2500 50  0000 C CNN
F 1 "100R" V 8384 2500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8430 2500 50  0001 C CNN
F 3 "~" H 8500 2500 50  0001 C CNN
	1    8500 2500
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5DFDBE93
P 3600 4250
AR Path="/5DFDBE93" Ref="R?"  Part="1" 
AR Path="/5DF8C731/5DFDBE93" Ref="R4"  Part="1" 
F 0 "R4" V 3393 4250 50  0000 C CNN
F 1 "6k8" V 3484 4250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3530 4250 50  0001 C CNN
F 3 "~" H 3600 4250 50  0001 C CNN
	1    3600 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	3450 2400 3450 4250
Connection ~ 3450 4250
Wire Wire Line
	3450 4250 3450 4900
Wire Wire Line
	4250 2400 4250 2650
Wire Wire Line
	4100 2400 4250 2400
Connection ~ 4250 2400
Wire Wire Line
	4250 2400 4750 2400
Wire Wire Line
	6150 2500 6350 2500
Wire Wire Line
	6350 2500 6350 2250
Connection ~ 6350 2500
Wire Wire Line
	6550 2500 6350 2500
Wire Wire Line
	7400 2500 7400 2400
Wire Wire Line
	6850 2500 7050 2500
Wire Wire Line
	8000 2500 8000 2650
Connection ~ 8000 2500
Wire Wire Line
	8000 2500 8350 2500
Wire Wire Line
	8000 3100 8000 3000
Wire Wire Line
	7050 2900 7050 2500
Connection ~ 7050 2500
Wire Wire Line
	7050 2500 7400 2500
Wire Wire Line
	8000 3400 7050 3400
Wire Wire Line
	7050 3400 7050 3200
Connection ~ 7050 3400
Wire Wire Line
	7050 3400 5950 3400
Wire Wire Line
	4250 3400 4250 2950
Wire Wire Line
	7400 2600 7300 2600
Wire Wire Line
	7300 2600 7300 3000
Wire Wire Line
	7300 3000 8000 3000
Connection ~ 8000 3000
Wire Wire Line
	8000 3000 8000 2950
Wire Wire Line
	6350 1950 6350 1800
Wire Wire Line
	6350 1800 8000 1800
Wire Wire Line
	8000 1800 8000 2500
Wire Wire Line
	3550 4900 3450 4900
Wire Wire Line
	3750 4250 4250 4250
Wire Wire Line
	4250 4250 4250 4300
Connection ~ 4250 4250
Wire Wire Line
	4250 4050 4250 4250
Wire Wire Line
	3550 5200 4250 5200
Wire Wire Line
	4250 5200 4250 4600
Connection ~ 4250 5200
Wire Wire Line
	4250 5300 4250 5200
Wire Wire Line
	4250 4050 4900 4050
Wire Wire Line
	3450 2400 3800 2400
Connection ~ 3450 2400
$Comp
L Device:C C?
U 1 1 5DFDBEE7
P 3300 2400
AR Path="/5DFDBEE7" Ref="C?"  Part="1" 
AR Path="/5DF8C731/5DFDBEE7" Ref="C5"  Part="1" 
F 0 "C5" V 3048 2400 50  0000 C CNN
F 1 "150nF" V 3139 2400 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3338 2250 50  0001 C CNN
F 3 "~" H 3300 2400 50  0001 C CNN
	1    3300 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	6750 3800 6750 3700
Wire Wire Line
	6750 3700 8150 3700
$Comp
L Connector:TestPoint TP?
U 1 1 5DFDBEEF
P 2850 2400
AR Path="/5DFDBEEF" Ref="TP?"  Part="1" 
AR Path="/5DF8C731/5DFDBEEF" Ref="TP1"  Part="1" 
F 0 "TP1" H 2908 2518 50  0000 L CNN
F 1 "TestPoint" H 2908 2427 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_2.0x2.0mm_Drill1.0mm" H 3050 2400 50  0001 C CNN
F 3 "~" H 3050 2400 50  0001 C CNN
	1    2850 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2400 2850 2400
Connection ~ 2850 2400
Wire Wire Line
	2650 2400 2850 2400
$Comp
L Connector:TestPoint TP?
U 1 1 5DFDBEF8
P 8750 2500
AR Path="/5DFDBEF8" Ref="TP?"  Part="1" 
AR Path="/5DF8C731/5DFDBEF8" Ref="TP2"  Part="1" 
F 0 "TP2" H 8808 2618 50  0000 L CNN
F 1 "TestPoint" H 8808 2527 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_2.0x2.0mm_Drill1.0mm" H 8950 2500 50  0001 C CNN
F 3 "~" H 8950 2500 50  0001 C CNN
	1    8750 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 2500 8650 2500
Connection ~ 8750 2500
Wire Wire Line
	8950 2500 8750 2500
Text GLabel 2650 2400 0    50   Input ~ 0
CROSSOVER_IN
Text GLabel 8950 2500 2    50   Output ~ 0
CROSSOVER_OUT_HIGH
$Comp
L Amplifier_Operational:TL072 U?
U 3 1 5DFDBF03
P 2550 3650
AR Path="/5DFDBF03" Ref="U?"  Part="3" 
AR Path="/5DF8C731/5DFDBF03" Ref="U1"  Part="3" 
F 0 "U1" H 2508 3696 50  0000 L CNN
F 1 "TL072" H 2508 3605 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 2550 3650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 2550 3650 50  0001 C CNN
	3    2550 3650
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL072 U?
U 3 1 5DFDBF09
P 2950 3650
AR Path="/5DFDBF09" Ref="U?"  Part="3" 
AR Path="/5DF8C731/5DFDBF09" Ref="U2"  Part="3" 
F 0 "U2" H 2908 3696 50  0000 L CNN
F 1 "TL072" H 2908 3605 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 2950 3650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 2950 3650 50  0001 C CNN
	3    2950 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 3350 2450 3150
Wire Wire Line
	2450 3950 2450 4200
Wire Wire Line
	2650 4200 2650 4250
Wire Wire Line
	2450 4200 2650 4200
Connection ~ 2650 4200
Wire Wire Line
	2650 4200 2850 4200
Wire Wire Line
	2850 4200 2850 3950
Wire Wire Line
	2850 3150 2850 3350
Text GLabel 9000 4150 2    50   Output ~ 0
CROSSOVER_OUT_LOW
Wire Wire Line
	9000 4150 8800 4150
Connection ~ 8800 4150
Wire Wire Line
	8800 4150 8700 4150
$Comp
L Connector:TestPoint TP?
U 1 1 5DFDBF2B
P 8800 4150
AR Path="/5DFDBF2B" Ref="TP?"  Part="1" 
AR Path="/5DF8C731/5DFDBF2B" Ref="TP3"  Part="1" 
F 0 "TP3" H 8858 4268 50  0000 L CNN
F 1 "TestPoint" H 8858 4177 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_2.0x2.0mm_Drill1.0mm" H 9000 4150 50  0001 C CNN
F 3 "~" H 9000 4150 50  0001 C CNN
	1    8800 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 3700 8150 4150
Wire Wire Line
	7000 5300 5900 5300
Connection ~ 7000 5300
Wire Wire Line
	7000 5300 7000 4750
Wire Wire Line
	8150 4150 8400 4150
Connection ~ 8150 4150
Wire Wire Line
	8150 5000 8150 4750
Wire Wire Line
	8150 4750 8150 4700
Connection ~ 8150 4750
Wire Wire Line
	8150 4750 7550 4750
Wire Wire Line
	7550 4750 7550 4250
Wire Wire Line
	8150 4150 8150 4400
Wire Wire Line
	7350 4050 7550 4050
Wire Wire Line
	7350 4150 7350 4450
Connection ~ 7350 4150
Wire Wire Line
	7350 4050 7350 4150
Wire Wire Line
	7350 4150 7250 4150
Wire Wire Line
	7000 4450 7350 4450
Wire Wire Line
	6750 4150 6950 4150
Connection ~ 6750 4150
Wire Wire Line
	6750 4100 6750 4150
Wire Wire Line
	6500 4150 6750 4150
$Comp
L Device:R R?
U 1 1 5DFDBF4D
P 7100 4150
AR Path="/5DFDBF4D" Ref="R?"  Part="1" 
AR Path="/5DF8C731/5DFDBF4D" Ref="R9"  Part="1" 
F 0 "R9" V 6893 4150 50  0000 C CNN
F 1 "6k8" V 6984 4150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7030 4150 50  0001 C CNN
F 3 "~" H 7100 4150 50  0001 C CNN
	1    7100 4150
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5DFDBF53
P 6350 4150
AR Path="/5DFDBF53" Ref="R?"  Part="1" 
AR Path="/5DF8C731/5DFDBF53" Ref="R7"  Part="1" 
F 0 "R7" V 6143 4150 50  0000 C CNN
F 1 "6k8" V 6234 4150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6280 4150 50  0001 C CNN
F 3 "~" H 6350 4150 50  0001 C CNN
	1    6350 4150
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5DFDBF59
P 8550 4150
AR Path="/5DFDBF59" Ref="R?"  Part="1" 
AR Path="/5DF8C731/5DFDBF59" Ref="R15"  Part="1" 
F 0 "R15" V 8343 4150 50  0000 C CNN
F 1 "100R" V 8434 4150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8480 4150 50  0001 C CNN
F 3 "~" H 8550 4150 50  0001 C CNN
	1    8550 4150
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5DFDBF5F
P 8150 4550
AR Path="/5DFDBF5F" Ref="R?"  Part="1" 
AR Path="/5DF8C731/5DFDBF5F" Ref="R12"  Part="1" 
F 0 "R12" H 8220 4596 50  0000 L CNN
F 1 "10k" H 8220 4505 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8080 4550 50  0001 C CNN
F 3 "~" H 8150 4550 50  0001 C CNN
	1    8150 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5DFDBF65
P 7000 4600
AR Path="/5DFDBF65" Ref="C?"  Part="1" 
AR Path="/5DF8C731/5DFDBF65" Ref="C11"  Part="1" 
F 0 "C11" H 6885 4554 50  0000 R CNN
F 1 "10n" H 6885 4645 50  0000 R CNN
F 2 "" H 7038 4450 50  0001 C CNN
F 3 "~" H 7000 4600 50  0001 C CNN
	1    7000 4600
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5DFDBF6B
P 6750 3950
AR Path="/5DFDBF6B" Ref="C?"  Part="1" 
AR Path="/5DF8C731/5DFDBF6B" Ref="C10"  Part="1" 
F 0 "C10" H 6635 3904 50  0000 R CNN
F 1 "10n" H 6635 3995 50  0000 R CNN
F 2 "" H 6788 3800 50  0001 C CNN
F 3 "~" H 6750 3950 50  0001 C CNN
	1    6750 3950
	-1   0    0    1   
$EndComp
$Comp
L Amplifier_Operational:TL072 U?
U 2 1 5DFDBF71
P 7850 4150
AR Path="/5DFDBF71" Ref="U?"  Part="2" 
AR Path="/5DF8C731/5DFDBF71" Ref="U2"  Part="2" 
F 0 "U2" H 7850 4517 50  0000 C CNN
F 1 "TL072" H 7850 4426 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 7850 4150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 7850 4150 50  0001 C CNN
	2    7850 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DFDBF77
P 8150 5150
AR Path="/5DFDBF77" Ref="R?"  Part="1" 
AR Path="/5DF8C731/5DFDBF77" Ref="R13"  Part="1" 
F 0 "R13" H 8220 5196 50  0000 L CNN
F 1 "10k" H 8220 5105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8080 5150 50  0001 C CNN
F 3 "~" H 8150 5150 50  0001 C CNN
	1    8150 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 5300 7000 5300
Wire Wire Line
	2450 3150 2650 3150
$Comp
L power:GND #PWR?
U 1 1 5E09059D
P 5950 3400
F 0 "#PWR?" H 5950 3150 50  0001 C CNN
F 1 "GND" H 5955 3227 50  0000 C CNN
F 2 "" H 5950 3400 50  0001 C CNN
F 3 "" H 5950 3400 50  0001 C CNN
	1    5950 3400
	1    0    0    -1  
$EndComp
Connection ~ 5950 3400
Wire Wire Line
	5950 3400 4250 3400
$Comp
L power:GND #PWR?
U 1 1 5E090CA2
P 5900 5300
F 0 "#PWR?" H 5900 5050 50  0001 C CNN
F 1 "GND" H 5905 5127 50  0000 C CNN
F 2 "" H 5900 5300 50  0001 C CNN
F 3 "" H 5900 5300 50  0001 C CNN
	1    5900 5300
	1    0    0    -1  
$EndComp
Connection ~ 5900 5300
Wire Wire Line
	5900 5300 4250 5300
$Comp
L power:-12V #PWR?
U 1 1 5E091874
P 2650 4250
F 0 "#PWR?" H 2650 4350 50  0001 C CNN
F 1 "-12V" H 2665 4423 50  0000 C CNN
F 2 "" H 2650 4250 50  0001 C CNN
F 3 "" H 2650 4250 50  0001 C CNN
	1    2650 4250
	-1   0    0    1   
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5E093A51
P 2650 3150
F 0 "#PWR?" H 2650 3000 50  0001 C CNN
F 1 "+12V" H 2665 3323 50  0000 C CNN
F 2 "" H 2650 3150 50  0001 C CNN
F 3 "" H 2650 3150 50  0001 C CNN
	1    2650 3150
	1    0    0    -1  
$EndComp
Connection ~ 2650 3150
Wire Wire Line
	2650 3150 2850 3150
$EndSCHEMATC
